+++
date = "2021-07-26"
title = "placeholder"
type = "misc"
summary = "summary summary summary."
+++

The Prose Edda, also known as the Younger Edda, Snorri's Edda (Icelandic: Snorra Edda) or, historically, simply as Edda, is an Old Norse textbook written in Iceland during the early 13th century. The work is often assumed to have been to some extent written, or at least compiled, by the Icelandic scholar, lawspeaker, and historian Snorri Sturluson c. 1220. It is considered the fullest and most detailed source for modern knowledge of Norse mythology, the body of myths of the North Germanic peoples, and draws from a wide variety of sources, including versions of poems that survive into today in a collection known as the Poetic Edda.