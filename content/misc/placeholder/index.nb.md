+++
date = "2021-07-26"
title = "plassholder"
type = "misc"
summary = "plassholder."
+++

Den yngre Edda ble skrevet av Snorre Sturlason omkring år 1220. Boken er en lærebok i skaldskap og er overlevert mer eller mindre i sin helhet i en rekke håndskrifter. Den yngre Edda er en av de viktigste kildene vi har til norrøn mytologi og hvilke versemål, rytme og rimmønster som brukes i norrøn diktning.

Dialogformen i Den yngre Edda er svært tydelig i bokens første to deler. Dette var fast tradisjon i pedagogiske tekster i antikken og middelalderen. Verket består først av en prolog og deretter tre hoveddeler