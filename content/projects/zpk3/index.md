+++
image = "list.png"
date = "2020-10-09"
title = "ZPK3"
type = "gallery"
+++

Repo currently private 

The most recent version of my university honours project, a 2D multiplayer space RPG. Whilst the original was full-JS, this one is currently being written to use a Godot client and a Golang server. The original also had gameplay, this verison is currently limited to players flying around different empty solar systems.

My main takeaway from this so far, which I expected going in, is that Go might not be the best language for video games due to its non-OOP nature, there have been lots of times where it'd have been nice to stick shared properties and behaviour on a base class that game objects can inherit from. Go interfaces and nested structs get you a decent amount of abstraction but it's not convenient. 

I don't think of this as a negative though since in web projects, which I mostly work on, it serves to prevent abstraction hell.

!["Gameplay"](gameplay.png)