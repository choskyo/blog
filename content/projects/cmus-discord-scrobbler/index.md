+++
image = "code.png"
date = "2020-12-11"
title = "CMUS-DISCORD-SCROBBLER"
type = "gallery"
summary = "Golang cli tool for subjecting discord friends to knowledge of your cmus-based music taste."
+++

[Repo](https://gitlab.com/choskyo/cmus-discord-scrobbler)

Simple Go CLI tool intended to be run as a daemon/whenever you want to share what music you're listening to with people on Discord.

Works by executing the `cmus-remote -Q` command to get info then parses its stdout and posts it to discord as an activity.

~~As with any Go program it's very easy to build + install, but I don't recommend it because it only got as far as "functional". There's a hardcoded user email/password in main.go until I get around to improving that. The DiscordGO docs mention that automation using email/pw auth can result in an account ban but I've been running this fine for 4 months and don't know anyone else who wants this so motivation on fixing it has been low.~~

Now re-written to be a simple run + it "just works". In the off-chance the discord app registration goes away for any reason or if you want to customise how it displays yourself, just replace the ApplicationID in the first line of the program with your own.