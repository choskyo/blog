+++
image = "chat.png"
date = "2020-07-15"
title = "TTME"
type = "gallery"
+++

- [API Repo](https://gitlab.com/choskyo/ttme2-api)
- [UI Repo](https://gitlab.com/choskyo/ttme2-react)

## What it does

TTME (the acronym means nothing btw) is a *currently* just generic chat site for anonymous paired conversations.

## Why I'm making it

All existing chat sites that I've seen on the internet (admittedly not *too* many) are complete garbage with no redeeming features other than that they probably run perfectly on IE6, they're also often run by shady companies and loaded with adware, so I'd like to provide an open source alternative people could easily host and use. This project is nowhere near there yet unfortunately, whilst it is functional for its primary use, it needs at a minimum proper moderation features added to prevent and punish abuse. 

This was also my first time using a redis cache, which I just wanted to give a shot so I knew how to for interviews/future jobs. Doing so has given me the inspiration to also use this project to try pub/sub messaging so that multiple instances can be deployed (for eg. on a k8s cluster) and horizontally scale, it currently can't do this since users are tied too tightly to a single node's websocket server.

*In addition* though on a much smaller note, I hadn't yet bothered with React hooks (class syntax works fine, I hate languages + frameworks that implement multiple ways to do the same thing), this project is the first time I had learned how to use them properly.

## How it works

![Queue](queue.png)
> User waiting in the queue, when someone else joins, a Go channel running on a ticker will pair the 2 together and they will be automatically moved to the chat page.

A react app combined with a Go API, nothing special about the UI, has light/darkmode, standard React/Redux, uses React Bootstrap for convenience, inbound websocket messages often dispatch Redux actions which worked surprisingly well. It's something I'm surprised I've never seen done commercially before and intend to bring into work whenever the opportunity seems right.

The API's primary libraries are [Gin](https://github.com/gin-gonic/gin) for REST endpoints and [Melody](https://github.com/olahol/melody/tree/v1) for websockets. Gin just reduces the amount of boilerplate-ish code that has to be written for a standard REST API in Go, and Melody saved a lot of time since despite being a bit buggy, it was the most "high-level" websocket library I could find. Creating my own websocket handling package could be interesting but was not what I felt like doing for this project.

```
const InputBar = () => {
	const [message, setMessage] = useState("");
	const [senderId, chatId] = useSelector((state: AppState) => {
		return [state.auth.sessionId, state.chat.id];
	});

	const sendMessage = () => {
		wsConn.send(
			`${MessageTypes.Chat}:${
				sessionStorage.getItem("token") || "asdf"
			}:${message}`
		);

		setMessage("");
	};

	const uploadImage = async (img: File) => {
		const f = new FormData();
		f.append("f", img);

		const h = new Map<string, string>();
		h.set("TTME-Sender", senderId);
		h.set("TTME-Conversation", chatId);

		await apiRequest("images", "v1", "upload", "POST", f, h, false);
	};

	return (
		<InputGroup className="fixed-bottom">
      ...
		</InputGroup>
	);
};

export default InputBar;
```
>Snippet showing how the client sends text messages and images to the server.

The above code snippet contains an `|| "asdf"` because I'm currently re-using a lot of the websocket code between this and another project which requires authentication. In that project the websocket messages are all in the format `${messageType}:${JWT}:${content}`, since there's no JWT here and I didn't feel like rewriting that part of the server, I just send along a random string and ignore it.

In terms of things "not working right", Melody would silently crash a connection every time I tried to send images across it, which is (the main reason) why image uploading is done via a hacky-ish method where the image is uploaded to a REST endpoint which then sends a message over websockets telling the appropriate clients that to request the image from another REST endpoint. Not elegant, but it works without problems and isn't too far from how I imagine most websites implement live notifications.

The Redis cache stores each user, a couple of details about them, and which conversation they're in (if any). The postgres DB currently doesn't store anything but is intended to be used for moderation and logging. There is a pubsub client which, as mentioned in the previous section, is intended to be used to accomodate horizontal scaling in future so that users can chat to each other when paired on different k8s nodes for example.

![Front page](./front_page.png)

## Learned from this?

- Gaining some understanding so far, with the intention of increasing this, of the extra work involved in systems that can horizontally scale properly.
- I now greatly prefer hooks over class syntax with some minor caveats, the syntax that replaces component lifecycle methods is far too opaque and impossible to know without looking up documentation. But overall it saves a tonne of code and specifically makes using Redux with Typescript way quicker to write.
- I started liking Bootstrap a lot less during this project, can't remember exactly why.