+++
image = "mddl.png"
date = "2020-07-15"
title = "MDDL"
type = "gallery"
+++

[Repo](https://gitlab.com/choskyo/mddl)

Originally a quick and messy program I threw together to help me archive fan translations for manga I was reading.

Started receiving a near-total re-write to use a proper structure and [framework](https://pkg.go.dev/github.com/urfave/cli/v2@v2.3.0) shortly before MangaDex itself went down, which has at-least temporarily killed this project.