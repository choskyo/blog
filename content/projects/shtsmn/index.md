+++
image = "home.png"
date = "2020-04-18"
title = "SHTSMN"
type = "gallery"
+++

[Repo](https://gitlab.com/choskyo/shtsmn)

SHTSMN is a Go server + React client combo that I built in response to people no longer being able to play the card game askhole with friends when COVID hit, feature-wise it basically recreates the experience of the grid game mode where a card is randomly picked and given to the next player in a cycle from among preset categories. Made redundant when the official version came out shortly after this one became publicly playable.

From a technical perspective I don't think there's anything particularly interesting to say about this project other than it was my first time trying to at least somewhat use the [gokit](https://gokit.io/) project structure. I've stuck with it ever since then with little changes here and there so far as I've not personally encountered any problems and find it very easy to write and test. 

I also think it was my first time ensuring I have a "scripts" folder to ensure a project's dev environment can be setup with essentially 0 effort, no more remembering what params to pass into each docker image etc. This proved to be very useful because after a year of not touching it, it only took about 5 minutes to setup and get some screenshots for writing this.

Some in-app screenshots below.

![Rooms List](rooms.png)

![In-game](ingame.png)