+++
image = "list.png"
date = "2021-03-01"
title = "MDDL-READER"
type = "gallery"
+++

[Repo](https://gitlab.com/choskyo/mddl-reader)

An electron/react/typescript/tailwind desktop client that works well with mddl for reading offline manga collections, my plan was to build in some ipc between the 2 then add progress syncing for sites like MAL/Anilist.

This was a nice chance to experiment with snowpack, worked well enough that I can't picture myself going back to webpack for any personal TS projects.

MangaDex went down a week or 2 after this project was started, killing mddl itself along with any motivation I had to work on a reader for it.

Below are some screenshots of it working.

![Chapter list](chapters.png)
MDDL knew what chapter + volume numbers were present in a collection, but didn't download chapter names, there was no need for them when all it had to do was sync files. Adding this was a to-do prior to MD going down.