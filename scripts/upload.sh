#!/bin/sh

hugo
aws s3 rm s3://choskyo.net --recursive --include "*"
aws s3 cp ./public/ s3://choskyo.net/ --recursive --acl public-read